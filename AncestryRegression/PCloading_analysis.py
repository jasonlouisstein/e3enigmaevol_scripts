### Depending on what kind of phase 1 files you have you may need to update sex in the fam files.
bfile=ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD
plink --bfile $bfile --update-sex update_sex 1 --make-bed --out $bfile.sex

### In my case I already had a file with updated sex:
bfile=ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD.sex
# Here I used on iids on unrelated individuals defined in the Unrelated iids defined in Gazal et al Scientific Reports 2015: 
# “we removed individuals from 227 relationships up to first-cousins detected by RELPAIR (see Table S3), 
# and the 94 individuals that have been inferred as first-cousin offspring or closer by FSuite.”

plink --bfile $bfile --keep unrelated_2261.txt --make-bed --out $bfile.2261unrel
bfile=ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD.sex.2261unrel
plink --bfile $bfile --indep-pairwise 500 100 0.2 --out $bfile
plink --bfile $bfile --extract $bfile.prune.in --make-bed --out $bfile.pruned1
plink --bfile $bfile.pruned1 --indep-pairwise 500 100 0.2 --out $bfile.pruned1
plink --bfile $bfile.pruned1 --extract $bfile.pruned1.prune.in --make-bed --out $bfile.pruned2

# running pca
bfile=ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD.sex.2261unrel.pruned2
plink --bfile $bfile --pca --out $bfile


### If you do not have a list of unrelated iids, you may want to do some relatedness analysis. I am also attaching the
# list of IIDS used by Robert Maier
## e.g.
#plink --bfile $bfile.pruned2 --genome --min 0.05 --out $bfile.pruned2
#plink --bfile $bfile.pruned2 --genome --min 0.125 --out $bfile.pruned2_genome0.125
#plink --bfile $bfile.pruned2 --genome --min 0.25 --out $bfile.pruned2_genome0.25

# running pca
bfile=ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD.pruned2
plink --bfile $bfile --pca --out $bfile




### This python script submits PC loading analysis for all 20PCs to different nodes. This is done on the non-pruned set
######################### script to submit PC loadings
import numpy as np
import os

for i in range(1):
    print i
    job = \
'''
#!/bin/bash
#PBS -N PC_loading'''+str(i)+'''
#PBS -S /bin/bash
#PBS -l walltime=40:30:00
#PBS -l nodes=1:ppn=1
#PBS -l mem=100gb
#PBS -o $HOME/logs/${PBS_JOBNAME}.o${PBS_JOBID}.log
#PBS -e $HOME/logs/${PBS_JOBNAME}.e${PBS_JOBID}.err

cd /gpfs/data/stranger-lab/eakhram/P3_1000GP/final_files/PCloadings/phase3_1kgp/unrel2261/
module load gcc/6.2.0
module load plink/1.90
bfile=ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD.sex.2261unrel
plink --bfile $bfile --linear --ci 0.95 --all-pheno --sex --pheno ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD.sex.2261unrel.pruned2.eigenvec --out PCloading.ns.allpop.unrel2261'''
    f = open('PCloading_ns_allpop.sh','w')
    f.write(job)
    f.close()
    os.system('qsub PCloading_ns_allpop.sh')


#### Combining files into one. Note that the naming could be different from above

### combine files into one
for i in $(seq 1 21):
do
    awk {'if($5=="ADD") print'} PCloading.ns.allpop.unrel2261.P$((i)).assoc.linear > PCloading.ns.allpop.unrel2261.P$((i)).assoc.linear.res
done


for i in $(seq 1 21):
do
    awk {'print $7,$8,$12'} PCloading.ns.allpop.unrel2261.P$((i)).assoc.linear.res > PCloading.ns.allpop.unrel2261.P$((i)).assoc.linear.res.short
done

paste ALL.chr1to22.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.all_pops.SNPskeep.maf0.05.MhcInvLongLD.sex.2261unrel.bim \
PCloading.ns.allpop.unrel2261.P1.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P2.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P3.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P4.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P5.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P6.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P7.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P8.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P9.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P10.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P11.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P12.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P13.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P14.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P15.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P16.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P17.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P18.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P19.assoc.linear.res.short \
PCloading.ns.allpop.unrel2261.P20.assoc.linear.res.short > 1kg_phase3_ns.allpop.unrel2261_eigenvec.P1to20_beta_se_pval.txt

sed -i '1iCHR SNP VAL POS A1 A2 P1beta P1se P1pval P2beta P2se P2pval P3beta P3se P3pval P4beta P4se P4pval P5beta P5se P5pval P6beta P6se P6pval P7beta P7se P7pval P8beta P8se P8pval P9beta P9se P9pval P10beta P10se P10pval P11beta P11se P11pval P12beta P12se P12pval P13beta P13se P13pval P14beta P14se P14pval P15beta P15se P15pval P16beta P16se P16pval P17beta P17se P17pval P18beta P18se P18pval P19beta P19se P19pval P20beta P20se P20pval' 1kg_phase3_ns.allpop.unrel2261_eigenvec.P1to20_beta_se_pval.txt
