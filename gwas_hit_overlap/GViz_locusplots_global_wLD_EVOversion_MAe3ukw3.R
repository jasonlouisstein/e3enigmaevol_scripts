## Use R from /usr/local/R-3.3.3/bin/R
## PATH=/usr/local/R-3.3.3/bin/:$PATH


## This version takes a list of SNPs and plots the surrounding area from 
# MAe3ukw3 data, plus: 
# Human Gained Enhancers (macaque and chimp)
# Human accelerated regions
# Neandertal Introgressed Regions
# Neandertal Depleted Regions
# Selective Sweeps

options(stringsAsFactors = FALSE)
library(Gviz)
library(biomaRt)

####################################################
## Setting up the Evolution-specific tracks
####################################################
beds <- read.delim("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/GViz/all_EVO_annots_MAe3ukw3.bed", stringsAsFactors = FALSE, header = FALSE)
colnames(beds) <- c("Chr", "Start", "End", "annot")
beds <- beds[!beds$Chr == "X", ]
beds$Chr <- as.numeric(beds$Chr)

HGE7 = beds[beds$annot == "HGE_7",]
HGE8 = beds[beds$annot == "HGE_8",]
HGE12F = beds[beds$annot == "HGE_12F",]
HGE12O = beds[beds$annot == "HGE_12O",]

macaque_HGE = beds[beds$annot == "macaque_HGE",]
macaque_HGP = beds[beds$annot == "macaque_HGP",]
chimp_HGE = beds[beds$annot == "chimp_HGE",]
chimp_HGP = beds[beds$annot == "chimp_HGP",]

sweeps = beds[beds$annot == "selective_sweep",]
Nean = beds[beds$annot == "Nean",]
Nean_del = beds[beds$annot == "Nean_Depleted",]
HAR = beds[beds$annot == "HAR",]

HGE7r = GRanges(HGE7$Chr, IRanges(HGE7$Start,HGE7$End))
HGE8r = GRanges(HGE8$Chr, IRanges(HGE8$Start,HGE8$End))
HGE12Fr = GRanges(HGE12F$Chr, IRanges(HGE12F$Start,HGE12F$End))
HGE12Or = GRanges(HGE12O$Chr, IRanges(HGE12O$Start,HGE12O$End))

macaque_HGEr = GRanges(macaque_HGE$Chr, IRanges(macaque_HGE$Start,macaque_HGE$End))
macaque_HGPr = GRanges(macaque_HGP$Chr, IRanges(macaque_HGP$Start,macaque_HGP$End))
chimp_HGEr = GRanges(chimp_HGE$Chr, IRanges(chimp_HGE$Start,chimp_HGE$End))
chimp_HGPr = GRanges(chimp_HGP$Chr, IRanges(chimp_HGP$Start,chimp_HGP$End))

sweepsr = GRanges(sweeps$Chr, IRanges(sweeps$Start,sweeps$End))
Neanr = GRanges(Nean$Chr, IRanges(Nean$Start,Nean$End))
Nean_delr = GRanges(Nean_del$Chr, IRanges(Nean_del$Start,Nean_del$End))
HARr = GRanges(HAR$Chr, IRanges(HAR$Start,HAR$End))

## Read in the IQ data
# load("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/GViz/diseasetrait/IQ/IQranges.Rdata")
# Specify which SNPs we want to make a plot for
evol_InputSNPs <- "rs1159974"

####################################################
## All Evo hits
####################################################

## File containing the independent hits for each region
fhits <- "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/GViz/Locus_number_251.csv"
## Location of meta-analyzed p-values
fMAres <- "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/QC5/"
# fGZCP <- "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3UKBB/ldscorescripts/partherit/differentialbinding_CQN_delatorrestein_cell.csv"
## 1000 Genomes data to estimate LD
fkgp <- "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/GViz/1000G_phase3/"
## 1000 Genomes data identifying European unrelated samples
fEUR <- "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/GViz/unrelatedEURids.txt"
## Turn the hits into genomic ranges file
hits <- read.csv(fhits)
colnames(hits) <- c("Locus.number", "PhenotypeNumber", "SA.TH", "RegionName", "SNP", "Chr", "BP", "A1", "A2", "Trait")
hits <- hits[hits$SNP %in% evol_InputSNPs, ]
# key line :)

hitsranges <- GRanges(hits$Chr, IRanges(hits$BP, hits$BP))
mcols(hitsranges) <- hits[, c(1:5, 8:10)]
## Parse the trait name to match the file name
hitsranges$filename <- "NA"
## If full thickness or SA, filename is
ind <- which(hitsranges$Trait == "Mean_Full_SurfArea")
hitsranges$filename[ind] <- "mixed_se_wo_Mean_Full_SurfArea1_e3ukw3_rs.txt"
ind <- which(hitsranges$Trait == "Mean_Full_Thickness")
hitsranges$filename[ind] <- "mixed_se_wo_Mean_Full_Thickness1_e3ukw3_rs.txt"
## If a regional SA
ind <- grep("surfavg", hitsranges$Trait)
hitsranges$filename[ind] <- paste0("mixed_se_wSA_", hitsranges$Trait[ind], "1_e3ukw3_rs.txt")
## If a region TH
ind <- grep("thickavg", hitsranges$Trait)
hitsranges$filename[ind] <- paste0("mixed_se_wTHICK_", hitsranges$Trait[ind], "1_e3ukw3_rs.txt")
## Subset to only global SA hits (Amanda note: skipping this!)
# ind = which(hitsranges$Trait=="Mean_Full_SurfArea");
# hitsranges = hitsranges[ind];

## Cytoband locations for ideogram downloaded here: http://hgdownload.cse.ucsc.edu/goldenpath/hg19/database/
data <- read.table("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/GViz/cytoBandIdeo.txt.gz", header = F, sep = "\t")
colnames(data) <- c("chrom", "chromStart", "chromEnd", "name", "gieStain")
# GZCP <- read.csv(fGZCP, row.names = 1)
# ## Remove X chromosome for ease of use
# GZCP <- GZCP[which(GZCP$seqnames != "X"), ]
# GZCPranges <- GRanges(GZCP$seqnames, IRanges(GZCP$start, GZCP$end))
# mcols(GZCPranges) <- GZCP[, c(4:10)]
# ## Find the ranges for GZ>CP or CP>GZ
# GZgreaterCP <- GZCPranges[which(mcols(GZCPranges)$padj < 0.05 & mcols(GZCPranges)$log2FoldChange > 0)]
# CPgreaterGZ <- GZCPranges[which(mcols(GZCPranges)$padj < 0.05 & mcols(GZCPranges)$log2FoldChange < 0)]
## Loop over each hit and get all SNPs p-values within a +/- 1Mb window for the trait
for (i in 1:length(hitsranges)) {
  ## Read in the meta-analyzed GWAS results
  pheno <- mcols(hitsranges[i])$Trait
  if (!file.exists(paste0("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/GViz/Rdatafiles/", pheno, ".Rdata"))) {
    cat("Reading Rdata file to save...\n")
    fMA <- paste0(fMAres, "/", hitsranges$filename[i])
    MA <- read.table(fMA, header = TRUE)
    ## Convert to genomic ranges and save as an Rdata file
    MAranges <- GRanges(MA$CHR, IRanges(MA$BP, MA$BP))
    mcols(MAranges) <- MA[, c(1:9)]
    save(MAranges, file = paste0("Rdatafiles/", pheno, ".Rdata"))
  } else {
    cat("loading in pre-existing Rdata file...\n")
    load(paste0("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/GViz/Rdatafiles/", pheno, ".Rdata"))
  }

  ## Calculate the LD
  system(paste0("/ifshome/smedland/bin/plink1.9 --bfile /ifshome/smedland/ARCHIEVE/refs/ALL.chr_merged.phase3_shapeit2_mvncall_integrated_v5.20130502.genotypes --r2  --ld-window-kb 10000 --ld-window 2000 --ld-window-r2 0.2 --ld-snp ", hitsranges[i]$SNP, " --out tmpld"))
  ## Read in the LD calculated from plink
  LD <- read.table("tmpld.ld", header = TRUE)

  ## Intersect a 1Mb range around the SNP of interest
  window <- resize(hitsranges[i], width = 1e6, fix = "center")
  olap <- findOverlaps(window, MAranges)
  MAregion <- MAranges[subjectHits(olap)]

  ## In order to color the different SNP dots differently, need to make overlay tracks for each possibility
  ldmatchind <- match(LD$SNP_B, MAregion$SNP)
  ldsnpsnocolor <- setdiff(1:length(MAregion), ldmatchind[which(!is.na(ldmatchind))])
  MAregionnocolor <- MAregion[ldsnpsnocolor]
  ## Add chr to seqnames
  MAregionnocolor <- renameSeqlevels(MAregionnocolor, paste0("chr", seqlevels(MAregionnocolor)))
  mcols(MAregionnocolor) <- data.frame(log10P = -log10(mcols(MAregionnocolor)$P))
  thischr <- as.character(seqnames(MAregionnocolor)[1])
  ## Make a data track for each SNP in the region as a p-value
  dtracknocolor <- DataTrack(MAregionnocolor, name = "-log10(P)", type = "p", legend = FALSE, col = "#192752", ylim = c(0.5, max(-log10(mcols(MAregion)$P)) * 1.1), baseline = -log10(5e-8), col.baseline = "grey", lty.baseline = 2, lwd.baseline = 1, genome = "hg19")

  ldmatchind <- match(LD$SNP_B[which(LD$R2 >= 0.8)], MAregion$SNP)
  MAregionred <- MAregion[ldmatchind[which(!is.na(ldmatchind))]]
  ## Add chr to seqnames
  MAregionred <- renameSeqlevels(MAregionred, paste0("chr", seqlevels(MAregionred)))
  mcols(MAregionred) <- data.frame(log10P = -log10(mcols(MAregionred)$P))
  dtrackred <- DataTrack(MAregionred, name = "-log10(P)", type = "p", legend = FALSE, col = "red", ylim = c(0.5, max(-log10(mcols(MAregion)$P)) * 1.1), baseline = -log10(5e-8), col.baseline = "grey", lty.baseline = 2, lwd.baseline = 1, genome = "hg19")

  ldmatchind <- match(LD$SNP_B[which(LD$R2 >= 0.6 & LD$R2 < 0.8)], MAregion$SNP)
  MAregionorange <- MAregion[ldmatchind[which(!is.na(ldmatchind))]]
  ## Add chr to seqnames
  MAregionorange <- renameSeqlevels(MAregionorange, paste0("chr", seqlevels(MAregionorange)))
  mcols(MAregionorange) <- data.frame(log10P = -log10(mcols(MAregionorange)$P))
  dtrackorange <- DataTrack(MAregionorange, name = "-log10(P)", type = "p", legend = FALSE, col = "orange", ylim = c(0.5, max(-log10(mcols(MAregion)$P)) * 1.1), baseline = -log10(5e-8), col.baseline = "grey", lty.baseline = 2, lwd.baseline = 1, genome = "hg19")

  ldmatchind <- match(LD$SNP_B[which(LD$R2 >= 0.4 & LD$R2 < 0.6)], MAregion$SNP)
  MAregiongreen <- MAregion[ldmatchind[which(!is.na(ldmatchind))]]
  ## Add chr to seqnames
  MAregiongreen <- renameSeqlevels(MAregiongreen, paste0("chr", seqlevels(MAregiongreen)))
  mcols(MAregiongreen) <- data.frame(log10P = -log10(mcols(MAregiongreen)$P))
  dtrackgreen <- DataTrack(MAregiongreen, name = "-log10(P)", type = "p", legend = FALSE, col = "green", ylim = c(0.5, max(-log10(mcols(MAregion)$P)) * 1.1), baseline = -log10(5e-8), col.baseline = "grey", lty.baseline = 2, lwd.baseline = 1, genome = "hg19")

  ldmatchind <- match(LD$SNP_B[which(LD$R2 >= 0.2 & LD$R2 < 0.4)], MAregion$SNP)
  MAregionlightblue <- MAregion[ldmatchind[which(!is.na(ldmatchind))]]
  ## Add chr to seqnames
  MAregionlightblue <- renameSeqlevels(MAregionlightblue, paste0("chr", seqlevels(MAregionlightblue)))
  mcols(MAregionlightblue) <- data.frame(log10P = -log10(mcols(MAregionlightblue)$P))
  dtracklightblue <- DataTrack(MAregionlightblue, name = "-log10(P)", type = "p", legend = FALSE, col = "lightblue", ylim = c(0.5, max(-log10(mcols(MAregion)$P)) * 1.1), baseline = -log10(5e-8), col.baseline = "grey", lty.baseline = 2, lwd.baseline = 1, genome = "hg19")

  # ## IQ region
  # olapi <- findOverlaps(window, IQranges)
  # IQregion <- IQranges[subjectHits(olapi)]
  # mcols(IQregion) <- data.frame(log10P = -log10(mcols(IQregion)$p_value))
  # dtracki <- DataTrack(IQregion, name = "IQ -log10(Pval)", type = "p", legend = FALSE, col = "#192752", ylim = c(0.5, max(mcols(IQregion)$log10P) * 1.1), baseline = -log10(5e-8), col.baseline = "grey", lty.baseline = 2, lwd.baseline = 1)

  ## start making a plot in Gviz
  itrack <- IdeogramTrack(genome = "hg19", chromosome = thischr, bands = data)
  gtrack <- GenomeAxisTrack()
  otcolors <- OverlayTrack(trackList = list(dtracknocolor, dtrackred, dtrackorange, dtrackgreen, dtracklightblue))

  mart <- useMart(biomart = "ENSEMBL_MART_ENSEMBL", dataset = "hsapiens_gene_ensembl", host = "feb2014.archive.ensembl.org")
  gencodev19 <- BiomartGeneRegionTrack(genome = "hg19", biomart = mart, chromosome = thischr, start = start(window), end = end(window), showId = TRUE, geneSymbols = TRUE, transcriptAnnotation = "symbol", name = "", filter = list(with_ox_refseq_mrna = TRUE), fill = "#368C41", col = NULL, col.line = NULL, fontsize = 18)

  # ## Make an GZ>CP and CP>GZ track
  # olapg <- findOverlaps(window, GZgreaterCP)
  # gAnnotTrack <- AnnotationTrack(GZgreaterCP[subjectHits(olapg)], genome = "hg19", name = "GZ>CP", col = NULL, col.line = NULL, stacking = "dense", fill = "#D7706A")
  # olapc <- findOverlaps(window, CPgreaterGZ)
  # cAnnotTrack <- AnnotationTrack(CPgreaterGZ[subjectHits(olapc)], genome = "hg19", name = "CP>GZ", col = NULL, col.line = NULL, stacking = "dense", fill = "#F28C87")

  ## Make Evo tracks
  olapevo1 = findOverlaps(window, HGE7r);
  HGE7rAnnotTrack = AnnotationTrack(HGE7r[subjectHits(olapevo1)],genome="hg19",name="HGE_7",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo2 = findOverlaps(window, HGE8r);
  HGE8rAnnotTrack = AnnotationTrack(HGE8r[subjectHits(olapevo2)],genome="hg19",name="HGE_8",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo3 = findOverlaps(window, HGE12Fr);
  HGE12FrAnnotTrack = AnnotationTrack(HGE12Fr[subjectHits(olapevo3)],genome="hg19",name="HGE_12F",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo4 = findOverlaps(window, HGE12Or);
  HGE12OrAnnotTrack = AnnotationTrack(HGE12Or[subjectHits(olapevo4)],genome="hg19",name="HGE_12O",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo5 = findOverlaps(window, macaque_HGEr);
  macaque_HGErAnnotTrack = AnnotationTrack(macaque_HGEr[subjectHits(olapevo5)],genome="hg19",name="macaque_HGE",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo6 = findOverlaps(window, macaque_HGPr);
  macaque_HGPrAnnotTrack = AnnotationTrack(macaque_HGPr[subjectHits(olapevo6)],genome="hg19",name="macaque_HGP",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo7 = findOverlaps(window, chimp_HGEr);
  chimp_HGErAnnotTrack = AnnotationTrack(chimp_HGEr[subjectHits(olapevo7)],genome="hg19",name="chimp_HGE",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo8 = findOverlaps(window, chimp_HGPr);
  chimp_HGPrAnnotTrack = AnnotationTrack(chimp_HGPr[subjectHits(olapevo8)],genome="hg19",name="chimp_HGP",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo9 = findOverlaps(window, sweepsr);
  sweepsrAnnotTrack = AnnotationTrack(sweepsr[subjectHits(olapevo9)],genome="hg19",name="Sweeps",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo10 = findOverlaps(window, Neanr);
  NeanrAnnotTrack = AnnotationTrack(Neanr[subjectHits(olapevo10)],genome="hg19",name="NeanSNPs",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo11 = findOverlaps(window, Nean_delr);
  Nean_delrAnnotTrack = AnnotationTrack(Nean_delr[subjectHits(olapevo11)],genome="hg19",name="Nean_depleted",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");
  
  olapevo12 = findOverlaps(window, HARr);
  HARrAnnotTrack = AnnotationTrack(HARr[subjectHits(olapevo12)],genome="hg19",name="HAR",col=NULL,col.line=NULL,stacking="dense",fill="#995e07");

  ## Plot the tracks
  pdf(paste0("LocusID",mcols(hitsranges[i])$Locus.number, "_", mcols(hitsranges[i])$SNP, "_", mcols(hitsranges[i])$Trait, ".pdf"), width = 3.5, height = 4.5)
  #png("test.png", width = 3.5, height  = 7, units = "in", res = 300)
  plotTracks(c(itrack, gtrack, otcolors, gencodev19,
    HGE7rAnnotTrack, HGE8rAnnotTrack, HGE12FrAnnotTrack, HGE12OrAnnotTrack,
    macaque_HGErAnnotTrack, macaque_HGPrAnnotTrack,
    chimp_HGErAnnotTrack, chimp_HGPrAnnotTrack, HARrAnnotTrack,
    sweepsrAnnotTrack, NeanrAnnotTrack, Nean_delrAnnotTrack),
  from = start(window), 
  to = end(window),
  transcriptAnnotation = "symbol",
  add53 = TRUE, 
  showBandID = TRUE, 
  cex.bands = 0.5, 
  stackHeight = 0.3,
  background.title = "white", 
  col.axis = "black",
  col.title = "black",
  rotation.title = 90, 
  cex.title = 0.5, 
  cex.axis = 0.7, 
  just.group = "right",
  main = paste0(mcols(hitsranges[i])$RegionName, " ", mcols(hitsranges[i])$SA.TH, ": ", mcols(hitsranges[i])$SNP, "\nEffect Allele: ", toupper(hitsranges[i]$A1)),
  cex.main = 0.8, 
  collapseTranscripts = "meta"
  )
  dev.off()
}
