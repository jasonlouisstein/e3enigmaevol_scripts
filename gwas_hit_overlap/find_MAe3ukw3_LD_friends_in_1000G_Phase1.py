###

# For each GWAS hit:
# 1. store chr, rsid, which comparison in variables
# 2. run PLINK to get the LD list
# 3. Combine the SNP ID lists and preserve info on which SNP/comparison they came from
# 4. Check for overlaps with the various bed files using big R script!

import os, sys, time, glob, csv

resultDir = "/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/AnnotateGWAShits/MAe3ukw3/"

# STEPS 1-2
# Using the 1000G VCF files we have in resource DB, not sure if these have been manipulated...
plinkDir = "/data/workspaces/lag/shared_spaces/Resource_DB/resource/1000Genomes_Phase1_VCF/ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20110521/"
non_relatives = "/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/AnnotateGWAShits/unrelatedEURids.txt"


positions = "/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/AnnotateGWAShits/MA2/180402_ENIGMA3_MA2_GWAShits_nominal_forVCFtools_tester.txt"
os.chdir(resultDir)
with open(GWAS_hits,'r') as file1, open(positions, 'r') as file2:
	for line, pos in zip(file1, file2):
		line = line.strip()
		data = line.split("\t")
		print(data)
		mySNP = data[3] # Marker name (chr:bp format) in 1000G phase 1
		myRSid = data[4] # rs id
		mySNP_chr = data[1] # Which chromosome its on
		mySNP_GWAS = data[0]
		pos = pos.strip()
		print(pos)
		with open("single_pos_"+myRSid+"."+mySNP_GWAS+".txt", "w") as f:
		     #wr = csv.writer(f, delimiter="\t")
		     #wr.writerows(pos)
		     f.write("#CHR\tPOS\n"+pos)
		time.sleep(5)
		#The GWAS it was a hit in
		print("**************************************************")
		print("******* Running vcftools to find the LD friends of SNP "+mySNP+", which is associated with "+mySNP_GWAS)
		os.system("vcftools \
			--maf 0.01 \
			--gzvcf  "+plinkDir+"ALL.chr"+mySNP_chr+".phase1_release_v3.20101123.snps_indels_svs.genotypes.vcf.gz \
			--keep "+non_relatives+" \
			--hap-r2-positions single_pos_"+myRSid+"."+mySNP_GWAS+".txt \
			--min-r2 0.6 \
			--out "+resultDir+"/1KG.Phase1.EUR.chr."+str(mySNP_chr)+"."+myRSid+"."+mySNP_GWAS)
		print("**************************************************")
print("******* All done with the list of GWAS hits!!")

# --hap-r2-positions "+positions+" \
#			--hap-r2-positions single_pos_"+myRSid+"."+mySNP_GWAS+".txt \

#time.sleep(20)

# STEP 3
# os.chdir(resultDir)
# LD_files = glob.glob("*.ld")
# results = []
# results.append(['CHR_A','BP_A','SNP_A','CHR_B','BP_B','SNP_B','R2'])
# for i in LD_files:
# 	file_name = i.split(".") #since information is stored in the file name based on how we named them above
# 	with open(i, 'r') as file:
# 		next(file) #skip header
# 		for line in file:
# 			line = line.strip()
# 			data = line.split()		
# 			data = data.append(file_name[6]) #add the GWAS comparison
# 			results.append(data)
# print(results[:10])


# with open("ENIGMA3_MA2_LD_friends.csv","w") as f:
#     wr = csv.writer(f, delimiter="\t")
#     wr.writerows(results)

print("****** All done! *********")