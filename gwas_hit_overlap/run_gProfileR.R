library(tidyverse)
library(here)
# library(skimr)
library(gProfiler2)

# -----------------------------------------------------
# Load output of GWAS_hits_overlap_eQTLs.R
# -----------------------------------------------------
load(here("data", "GeneOntology", "eQTL_based", "adult_fetal_eQTLs_mapped_to_overlapping_SNPs.Rdata"))

# -----------------------------------------------------
# Prep gProfileR function
# Note: we're using version 0.6.7
# -----------------------------------------------------

run_gProfileR <- function(inputDF, annot, GWAS, trait) {
  gene_list <- inputDF %>%
    filter(str_detect(which_annots, annot)) %>%
    select(`ensg_id`) %>%
    unique()
  write.csv(gene_list, file = here("data", "GeneOntology", "eQTL_based", paste0(GWAS, "_", trait, "_", annot, "_genes.txt")), quote = F, row.names = F)
  GO <- gprofiler(gene_list$ensg_id, 
                  organism = "hsapiens", 
                  exclude_iea = TRUE, 
                  correction_method = "fdr", 
                  significant = TRUE, 
                  src_filter = c("GO:MF", "GO:CC", "GO:BP", "KEGG", "REAC"), 
                  ordered_query = FALSE, 
                  numeric_ns = "ENTREZGENE_ACC", 
                  max_p_value = 0.05, 
                  domain_size = "annotated")
  today <- Sys.Date()
  write.table(GO,
    file = here("data", "GeneOntology", "eQTL_based", paste0(GWAS, "_", trait, "_", annot, "_gProfileR_results_", today, ".txt")),
    quote = F, row.names = F, sep = "\t"
  )
  return(GO)
}

# -----------------------------------------------------
# Test it.
# -----------------------------------------------------

test <- gprofiler(full_SA_HGE_7$ensg_id, 
          organism = "hsapiens", 
          exclude_iea = TRUE, 
          correction_method = "fdr", 
          significant = TRUE, 
          src_filter = c("GO", "KEGG", "REAC"), 
          ordered_query = FALSE, 
          numeric_ns = "ENTREZGENE_ACC", 
          # max_p_value = 0.05, 
          domain_size = "annotated")





test <- run_gProfileR(adult_full_SA, "HGE_7", "Full", "SA")
