#!/bin/tcsh

set ftrait = $1
set foutput = $2

set trait = `basename $ftrait`

echo "*** Beginning LDSC ..."
echo "Trait: $trait"
echo "Output: $foutput"

/ifshome/smedland/bin/anaconda2/bin/python /ifshome/smedland/bin/ldsc/ldsc.py  --h2 ${ftrait} --out ${foutput} --ref-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/ --w-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/


echo "Finished!"

## Running height individually
# /ifshome/smedland/bin/anaconda2/bin/python /ifshome/smedland/bin/ldsc/ldsc.py  \
# --h2 /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/sumstats/HEIGHT_ancreg_1KGP3_ancreg.txt.sumstats.gz \
# --out /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/HEIGHT_ancreg_1KGP3_ancreg.txt.sumstats.gz \
# --ref-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/ \
# --w-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/


# /ifshome/smedland/bin/anaconda2/bin/python /ifshome/smedland/bin/ldsc/ldsc.py  \
# --h2 /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/sumstats/GIANT_HEIGHT_Wood_et_al_2014_publicrelease_HapMapCeuFreq.sumstats.gz \
# --out /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/GIANT_HEIGHT_Wood_et_al_2014_publicrelease_HapMapCeuFreq.sumstats.gz \
# --ref-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/ \
# --w-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/


# /ifshome/smedland/bin/anaconda2/bin/python /ifshome/smedland/bin/ldsc/ldsc.py  \
# --h2 /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/sumstats/Mean_Full_SurfArea_ancreg_1KGP3_ancreg.txt.sumstats.gz \
# --out /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/Mean_Full_SurfArea_ancreg_1KGP3_ancreg.txt.sumstats.gz \
# --ref-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/ \
# --w-ld-chr /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/resources/eur_w_ld_chr/