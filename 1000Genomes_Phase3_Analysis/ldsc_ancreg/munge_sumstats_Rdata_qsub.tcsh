#!/bin/tcsh
# Generate jobs to run munge_sumstats.py for the last version of the ENIGMA3-UKBB GWAS
# This script uses the GC CORRECTED version of the summary statistics, taken from the Rdata
# files produced by Jason on Jan 2 2019, so they're completely matched in terms of the number
# of SNPs to the ancestry regressed version of the summary statistics. These files contain
# summary statistics that are NOT ANCESTRY CORRECTED, but based on the original BETA columns

foreach E3MA (`cat /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/premunge_sumstats_from_Rdata/1KGPhase3_nonancreg_Rdata_w_gc_correction_premunge_sumstats_filenames.txt`) 
	set baseE3MA = `basename $E3MA`
	echo $baseE3MA
	## Make an output directory
	sed 's/E3MA/'$baseE3MA'/g' munge_sumstats_Rdata_slave.tcsh > /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	#echo "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/partherit/scripts/munge_sumstats_slave.tcsh $E3MA ${outputdir}" > /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/partherit/scripts/munge_sumstats/${baseE3MA}.sh
	chmod a+x /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	if (! -f /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/sumstats_from_Rdata/${baseE3MA}.log) then
	    qsub -j y /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	endif
end



### Height before ancreg: /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/GIANT/GIANT_HEIGHT_Wood_et_al_2014_publicrelease_HapMapCeuFreq.txt