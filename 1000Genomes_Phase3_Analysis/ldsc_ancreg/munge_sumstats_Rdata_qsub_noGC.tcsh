#!/bin/tcsh
# Generate jobs to run munge_sumstats.py for the last version of the ENIGMA3-UKBB GWAS
# This script uses the NON-GC CORRECTED version of the summary statistics, taken from the Rdata
# files produced by Jason on Jan 2 2019. It runs both the ancestry regressed (based on ancBETA), 
# and non-ancestry regressed versions, in prep for a final run of LDSC. 


# For the ancestry regressed versions
foreach E3MA (`cat /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/premunge_sumstats/no_GC_version/1KGPhase3_ancreg_noGC_premunge_sumstats_filenames.txt`) 
	set baseE3MA = `basename $E3MA`
	echo $baseE3MA
	sed 's/E3MA/'$baseE3MA'/g' munge_sumstats_Rdata_slave_noGC_w_ancreg.tcsh > /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	chmod a+x /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	if (! -f /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/sumstats/sumstats_noGC/${baseE3MA}.log) then
	    qsub -j y /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	endif
end

# For the ancestry regressed versions
foreach E3MA (`cat /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/premunge_sumstats/no_GC_version/1KGPhase3_nonancreg_noGC_premunge_sumstats_filenames.txt`) 
	set baseE3MA = `basename $E3MA`
	echo $baseE3MA
	sed 's/E3MA/'$baseE3MA'/g' munge_sumstats_Rdata_slave_noGC_wo_ancreg.tcsh > /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	chmod a+x /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	if (! -f /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/sumstats/sumstats_noGC/${baseE3MA}.log) then
	    qsub -j y /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	endif
end

