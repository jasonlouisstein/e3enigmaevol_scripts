#!/bin/tcsh
# Generate jobs to run munge_sumstats.py for the last version of the ENIGMA3-UKBB GWAS

foreach E3MA (`cat /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/premunge_sumstats/1KGPhase3_ancreg_premunge_sumstats_filenames.txt`) 
	set baseE3MA = `basename $E3MA`
	echo $baseE3MA
	## Make an output directory
	sed 's/E3MA/'$baseE3MA'/g' munge_sumstats_slave.tcsh > /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	#echo "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/partherit/scripts/munge_sumstats_slave.tcsh $E3MA ${outputdir}" > /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/partherit/scripts/munge_sumstats/${baseE3MA}.sh
	chmod a+x /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	if (! -f /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/sumstats/${baseE3MA}.log) then
	    qsub -j y /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/scripts/munge_sumstats/${baseE3MA}.sh
	endif
end



### Height before ancreg: /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/GIANT/GIANT_HEIGHT_Wood_et_al_2014_publicrelease_HapMapCeuFreq.txt