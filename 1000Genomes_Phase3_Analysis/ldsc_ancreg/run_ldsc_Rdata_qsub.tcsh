#!/bin/tcsh

# This script loops over each annotation for each set of summary statistics (e.g. global surface area, global thickness), 
# makes a directory for the results, and makes new 'slave' scripts from the template. At the end, it checks if there are
# already results files for that set of sumstats, and if not, the edited slave script is sent to the queue.


## Genreating LDSC results for sumstats without ancestry regression, but WITH GC Correction
# This was updated after realizing that we needed to trim the sumstats to just SNPs that also appear in the ancestry 
# regressed version so that it's a fair comparison. This was done by using the same Rdata file that Jason made on Jan 2, 
# 2019, but using the BETA columns from before the ancestry regression was added. 

foreach E3MA (`cat /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/1000G_phase3_non_ancreg_Rdata_sumstats.txt`) 
	set baseE3MA = `basename $E3MA`
	## Make an output directory
	set outputdir = /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/${baseE3MA}
	mkdir -p ${outputdir}
	echo "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/run_ldsc_slave.tcsh $E3MA ${outputdir}" > scripts/ldsc_MAe3ukw3_${baseE3MA}.sh
	chmod a+x scripts/ldsc_MAe3ukw3_${baseE3MA}.sh
	if (! -f /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/${baseE3MA}.results) then
	    qsub -o `pwd`/shelloutput/ldsc_MAe3ukw3_${baseE3MA}.out -j y `pwd`/scripts/ldsc_MAe3ukw3_${baseE3MA}.sh
	endif
end





#the end!