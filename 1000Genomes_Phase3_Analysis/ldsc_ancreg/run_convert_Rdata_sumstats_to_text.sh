#!/bin/bash
#$ -o /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/qsub_logs/


## Need to run this Rscript on the USC grid, because otherwise it fails due to broken pipes

cd /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/

/usr/local/R-3.3.3/bin/Rscript /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/convert_Rdata_sumstats_to_text_noGC.R