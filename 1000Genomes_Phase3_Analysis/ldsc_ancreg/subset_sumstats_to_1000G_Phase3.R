# ---------------------------------------------
# Subsetting the non-ancestry regressed ENIGMA
# summary statistics by 1000G Phase 3, so that 
# the final set of SNPs going into LDSC is the 
# same in both the "before" and "after" versions
#
# This should happen after munging the sumstats
# using munge_sumstats_qsub.tsch.
# ---------------------------------------------
library(data.table)

before_sumstats <- Sys.glob("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/ldscoremunged/*_w.sumstats.gz")
after_sumstats <- Sys.glob("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/sumstats/Mean*1KGP3_ancreg.txt.sumstats.gz")


for (i in 1:length(before_sumstats)){
  before_name <- basename(before_sumstats[i])
  before <-fread(before_sumstats[i])
  after <- fread(after_sumstats[i])
  before_small <- before[before$SNP %in% after$SNP,]
  difference <- nrow(before) - nrow(before_small)
  message(paste("We removed", difference, "SNPs from", before_name))
  gz1 <- gzfile(paste0("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/sumstats_1kgp3/",before_name), "w")
  write.table(before_small, gz1, quote = FALSE, sep = "\t", row.names = FALSE)
  close(gz1)
}


# Repeat for height, which is named differently across the two sets of sumstats
before_sumstats <- Sys.glob("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/sumstats/GIANT_HEIGHT_*.sumstats.gz")
after_sumstats <- Sys.glob("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/sumstats/HEIGHT*.sumstats.gz")


for (i in 1:length(before_sumstats)){
  before_name <- basename(before_sumstats[i])
  before <-fread(before_sumstats[i])
  after <- fread(after_sumstats[i])
  before_small <- before[before$SNP %in% after$SNP,]
  difference <- nrow(before) - nrow(before_small)
  message(paste("We removed", difference, "SNPs from", before_name))
  gz1 <- gzfile(paste0("/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/sumstats_1kgp3/",before_name), "w")
  write.table(before_small, gz1, quote = FALSE, sep = "\t", row.names = FALSE)
  close(gz1)
}

# To make a list of these files for use in the run_ldsc_qsub.tcsh file, do:
# cd /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/sumstats_1kgp3/
# ls -d -1 $PWD/* > ../../1000G_phase3_non_ancreg_sumstats.txt