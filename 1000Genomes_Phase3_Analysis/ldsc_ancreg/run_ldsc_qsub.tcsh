#!/bin/tcsh

# This script loops over each annotation for each set of summary statistics (e.g. global surface area, global thickness), 
# makes a directory for the results, and makes new 'slave' scripts from the template. At the end, it checks if there are
# already results files for that set of sumstats, and if not, the edited slave script is sent to the queue.


# Using the sumstats files that include correction for global thickness or surface area
#/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/E3ancreg1KGP3_CLUMPED/E3ancreg1KGP3text
#/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/1000Gphase3_PC_cor/AncestryRegressionData/GWASfiles.txt"
## Loop over each E3 GWAS
foreach E3MA (`cat /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/1000G_phase3_ancreg_sumstats.txt`) 
	set baseE3MA = `basename $E3MA`
	## Make an output directory
	set outputdir =  /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/${baseE3MA}
	mkdir -p ${outputdir}
	echo "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/run_ldsc_slave.tcsh $E3MA ${outputdir}" > scripts/ldsc_MAe3ukw3_ancreg_${baseE3MA}.sh
	chmod a+x scripts/ldsc_MAe3ukw3_ancreg_${baseE3MA}.sh
	if (! -f /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/w_1KGP3_ancreg/${baseE3MA}.results) then
	    qsub -o `pwd`/shelloutput/ldsc_MAe3ukw3_ancreg_${baseE3MA}.out -j y `pwd`/scripts/ldsc_MAe3ukw3_ancreg_${baseE3MA}.sh
	endif
end


## Genreating LDSC results for sumstats without ancestry regression
# This was updated after realizing that we needed to trim the sumstats to just SNPs that also appear in the ancestry 
# regressed version so that it's a fair comparison.
foreach E3MA (`cat /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/1000G_phase3_non_ancreg_sumstats.txt`) 
	set baseE3MA = `basename $E3MA`
	## Make an output directory
	set outputdir =  /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/only_1kgp3/${baseE3MA}
	mkdir -p ${outputdir}
	echo "/ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/run_ldsc_slave.tcsh $E3MA ${outputdir}" > scripts/ldsc_MAe3ukw3_${baseE3MA}.sh
	chmod a+x scripts/ldsc_MAe3ukw3_${baseE3MA}.sh
	if (! -f /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/LDSC/wo_1KGP3_ancreg/only_1kgp3/${baseE3MA}.results) then
	    qsub -o `pwd`/shelloutput/ldsc_MAe3ukw3_${baseE3MA}.out -j y `pwd`/scripts/ldsc_MAe3ukw3_${baseE3MA}.sh
	endif
end





#the end!