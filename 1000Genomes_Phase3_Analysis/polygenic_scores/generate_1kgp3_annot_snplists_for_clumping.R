
## The purpose of this script is to take Jason's Rdata files and
# generate a a list of SNPs that fall within a specific set of genomic regions.
# As of April 2019, we are using ancestry regressed data that includes only 
# SNPs from 1000 Genomes *Phase 3*. In November 2018 we had done a version that 
# used data from 1000 Genomes Phase 1, but we later updated to Phase 3 and now
# need to update the PGS. In mid-April we decided to use the non-GC corrected version.

# The next step in this pipeline is to upload these files to the USC server, 
# and run PLINK's --clump command. The .clumped output files contain the
# clumped SNpas a list that we can use to finish building the PGS.

# Note: it's important to do the clumping *after* subsetting the SNPs to 
# our annotation of interest, because we want the maximum  ach analysis. 
# If you start out with a list of clumped SNPs from the whole dataset, 
# there will be many fewer SNPs overlapping with the annotation. 

# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------
library(tidyverse)
library(GenomicRanges)
library(here)
library(diffloop)
here()
# -----------------------------------------------------------------------------
# Download the ancestral allele data and make a GRanges object
# -----------------------------------------------------------------------------

#ancestrals = getBM(mart = grch37, attributes = c("refsnp_id", "chr_name", "chrom_start", "chrom_end", "allele_1"), filters = "snp_filter", values = mergedGR$SNP)
#ancestrals_GR = makeGRangesFromDataFrame(ancestrals, keep.extra.columns = TRUE, seqnames.field = "chr_name", start.field = "chrom_start", end.field = "chrom_end")
#save(ancestrals_GR, file = "grch37_ancestral_alleles.Rdata", compress = TRUE)
load(here("data", "PGS", "grch37_ancestral_alleles.Rdata"))

# load the bed file for the regions of interest 

bed = read.table(here("data", "PGS", "7pcw_Hu_gain_merged.bed"))
bed_GR = makeGRangesFromDataFrame(bed, seqnames.field = "V1", start.field = "V2", end.field = "V3")
bed_GR = rmchr(bed_GR)


#Loop over all 70 sets of summary statistics, generating input files for PLINK's --clump flag. 
# PLINK needs the files to contain a header and columns for the SNP marker and p-value
# Important: ***The p-value is the ancestry regressed version***
# The actual clumping was run on the USC servers, with results located in: 
# /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/PGS/7pcw_clumped_1kgp3_noGC/.


files = Sys.glob(path = here("data", "sumstats","AncestryRegressionData_noGC", "Mean*.Rdata"))
for (j in 1:length(files)) {
  print(paste("working on file", basename(files[j])))
  load(files[j])
  ROI = subsetByOverlaps(mergedGR,bed_GR)
  snplist = as.data.frame(ROI)
  snp_filename <- paste0("7pcw_unclumped_noGC_",basename(files[j]),".snplist")
  write.table(snplist[,c(6,18)], file = here("data", "PGS", "7pcw_HGE_1kgp3_noGC_snplists", snp_filename), col.names = TRUE, row.names = FALSE, sep = " ", quote = FALSE)
}
