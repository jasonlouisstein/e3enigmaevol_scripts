for i in Mean_bankssts_surfavg Mean_bankssts_thickavg Mean_caudalanteriorcingulate_surfavg Mean_caudalanteriorcingulate_thickavg Mean_caudalmiddlefrontal_surfavg Mean_caudalmiddlefrontal_thickavg Mean_cuneus_surfavg Mean_cuneus_thickavg Mean_entorhinal_surfavg Mean_entorhinal_thickavg Mean_frontalpole_surfavg Mean_frontalpole_thickavg Mean_Full_SurfArea Mean_Full_Thickness Mean_fusiform_surfavg Mean_fusiform_thickavg Mean_inferiorparietal_surfavg Mean_inferiorparietal_thickavg Mean_inferiortemporal_surfavg Mean_inferiortemporal_thickavg Mean_insula_surfavg Mean_insula_thickavg Mean_isthmuscingulate_surfavg Mean_isthmuscingulate_thickavg Mean_lateraloccipital_surfavg Mean_lateraloccipital_thickavg Mean_lateralorbitofrontal_surfavg Mean_lateralorbitofrontal_thickavg Mean_lingual_surfavg Mean_lingual_thickavg Mean_medialorbitofrontal_surfavg Mean_medialorbitofrontal_thickavg Mean_middletemporal_surfavg Mean_middletemporal_thickavg Mean_paracentral_surfavg Mean_paracentral_thickavg Mean_parahippocampal_surfavg Mean_parahippocampal_thickavg Mean_parsopercularis_surfavg Mean_parsopercularis_thickavg Mean_parsorbitalis_surfavg Mean_parsorbitalis_thickavg Mean_parstriangularis_surfavg Mean_parstriangularis_thickavg Mean_pericalcarine_surfavg Mean_pericalcarine_thickavg Mean_postcentral_surfavg Mean_postcentral_thickavg Mean_posteriorcingulate_surfavg Mean_posteriorcingulate_thickavg Mean_precentral_surfavg Mean_precentral_thickavg Mean_precuneus_surfavg Mean_precuneus_thickavg Mean_rostralanteriorcingulate_surfavg Mean_rostralanteriorcingulate_thickavg Mean_rostralmiddlefrontal_surfavg Mean_rostralmiddlefrontal_thickavg Mean_superiorfrontal_surfavg Mean_superiorfrontal_thickavg Mean_superiorparietal_surfavg Mean_superiorparietal_thickavg Mean_superiortemporal_surfavg Mean_superiortemporal_thickavg Mean_supramarginal_surfavg Mean_supramarginal_thickavg Mean_temporalpole_surfavg Mean_temporalpole_thickavg Mean_transversetemporal_surfavg Mean_transversetemporal_thickavg

do

echo "$i"

echo "cd `pwd`" > $i.sh

head -1 /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/E3ancreg1KGP3_noGC_CLUMPED/E3ancreg1KGP3text/${i}.txt > ${i}_hgdp.snplist
awk 'NR==FNR {FILE1[$1]=$0; next} ($3 in FILE1) {print $0}' /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/Qx/Genome_Data/hgdp.snplist /ifs/loni/faculty/dhibar/ENIGMA3/MAe3ukw3/evolution/E3ancreg1KGP3_noGC_CLUMPED/E3ancreg1KGP3text/${i}.txt >> ${i}_hgdp.snplist
echo "/ifshome/smedland/bin/plink1.9 --bfile /ifshome/smedland/ARCHIEVE/refs/ALL.chr_merged.phase3_shapeit2_mvncall_integrated_v5.20130502.genotypes --clump ${i}_hgdp.snplist --clump-r2 .2 --clump-kb 100000 --clump-p1 1e-4 --out `pwd`/$i" >> $i.sh

chmod a+x $i.sh
qsub -o $i.out -j y `pwd`/$i.sh

done


