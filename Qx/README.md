#Qx analysis

To determine if alleles associated with brain structure show evidence of polygenic adaptation since the separation of modern human populations, we used the Qx score (https://www.ncbi.nlm.nih.gov/pubmed/25102153) to determine if modern human populations have different allele frequencies for brain structure associated SNPs compared to allele frequency differences expected by drift. This code was implemented for our purposes using the functions provided from the Qx repo (https://github.com/jjberg2/PolygenicAdaptationCode).

##Identifying LD independent SNPs impacting brain structure using clumping

Cortical SA GWAS ancestry regressed summary statistics were first intersected to the set of SNPs present in the HGDP dataset and then clumped to a set of LD-independent SNPs (r2 < 0.2; 1000G Phase 3 EUR population) at a nominal significance threshold (p-value < 1x10-4) using PLINK(v1.9). This is implemented in `clump.sh`.

##Implementing Qx

Allele frequencies in current human populations were downloaded from the Human Genome Diversity Project (http://plab-server.uchicago.edu). Background selection values were downloaded from previous work (https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1000471).The French population, a European population with similar allele frequencies to the cortical SA GWAS was used as the match population. Clumped SNPs with low minor allele frequency (MAF < 0.05) in the French population were removed from the dataset. Qx score functions were downloaded from https://github.com/jjberg2/PolygenicAdaptationCode and the parameters of PolygenicAdaptationFunction() were set to be identical to the example provided in the repository. The Qx p-value was recorded for global SA and each of the 34 gyrally defined regions. Benjamini Hochberg FDR correction was used to correct for multiple comparisons across each of the 35 GWASs used. This is implemented in `QxDriver.R` and `Qxslave.R`.

##Aggregating Qx stats

Qx stats were then read in for each region using `QxAggregateStats.R`.
