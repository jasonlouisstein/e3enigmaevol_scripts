# Updated file to work with 1000G Phase 3 data!
import os, sys
#----------------------------------------------------------------------
# Set up the list of chromosomes and other folders
#----------------------------------------------------------------------
resultsDir="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000_Genomes_Phase3/EpiRoadmap_E081_active_Phase3/"
oneKG="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/1000G_plinkfiles/"
hapmapdir="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/hapmap3_snps/"
LDSCdir="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/software/ldsc/"
os.chdir(resultsDir)


#----------------------------------------------------------------------
# Make the rest of the annotation files needed for running LDSC
# partitioned heritability. Command via this Wiki: 
# https://github.com/bulik/ldsc/wiki/LD-Score-Estimation-Tutorial
#----------------------------------------------------------------------
for i in range(1, 23):
	#print(i)
	print("Beginning LDScore calculation for chromosome "+str(i))
	os.system("python "+LDSCdir+"ldsc.py \
		--l2 --bfile "+oneKG+"1000G.mac5eur."+str(i)+" \
		--ld-wind-cm 1 \
		--annot "+resultsDir+str(i)+".annot.gz \
		--out "+resultsDir+str(i)+" \
		--print-snps "+hapmapdir+"hm."+str(i)+".snp")
	print("Done with LDScore calculation for chromosome "+str(i))

print("Done with the male fetal enhancers!")
print("***********************************")


resultsDir="/data/workspaces/lag/workspaces/lg-neanderthals/raw_data/ENIGMA-EVO/LDSC/EpiRoadmap_E082_active_Phase1/"
os.chdir(resultsDir)
for i in range(1, 23):
	#print(i)
	print("Beginning LDScore calculation for chromosome "+str(i))
	os.system("python "+LDSCdir+"ldsc.py \
		--l2 --bfile "+oneKG+"1000G.mac5eur."+str(i)+" \
		--ld-wind-cm 1 \
		--annot "+resultsDir+str(i)+".annot.gz \
		--out "+resultsDir+str(i)+" \
		--print-snps "+hapmapdir+"hm."+str(i)+".snp")
	print("Done with LDScore calculation for chromosome "+str(i))

print("Done with the female fetal enhancers!")
